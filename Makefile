CC=gcc
CFLAGS=-I. -Os -ffunction-sections -fdata-sections
LFLAGS=-Wl,--gc-sections
LIBS=-lX11 -lImlib2

STRIP=strip
STRIPFLAGS=-s -R .comment -R .gnu.version --strip-unneeded

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

xsplashscreen: main.o
	$(CC) -o xsplashscreen main.o $(LIBS) $(LFLAGS)
	$(STRIP) $(STRIPFLAGS) xsplashscreen

.PHONY: clean

clean:
	rm -f *.o xsplashscreen
