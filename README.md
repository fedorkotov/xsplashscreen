# XSplashScreen #

A small splashscreen program for your shell-based scripts and launchers with minimal dependencies (only X11 and Imlib2). 
It can either just show an image for a specified duration of time or
periodically check for tcp port availability until connection attempt 
succeeds

## Arguments ##

1. an absolute or relative path to image file supported by Imlib2 to be 
   displayed in the splashscreen window 
2. optional maximum delay in seconds before 
   the splashscreen window is closed and the program
   exits. Default value is 5 seconds
3. an optional hostname or address for port availability
   checks that are repeated every second if
   this parameter is specified. The script exits with 0
   return code as soon as connection attempt succeeds.
   If the parameter is specified but none of connection
   attempts before timeout succeeds, the program exits with
   return code of 2 after delay specified in second argument
4. optional port number. Default value is 80

Usage example:
```bash
./xsplashscreen testimage.png 5 example.com 22
```
