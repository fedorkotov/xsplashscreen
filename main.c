
// https://stackoverflow.com/questions/14995104/how-to-load-bmp-file-using-x11-window-background
// https://stackoverflow.com/questions/31361859/simple-window-without-titlebar
// https://stackoverflow.com/questions/8867715/xlib-center-window
// https://stackoverflow.com/questions/36014153/clean-exit-from-x11-application
// https://theredblacktree.wordpress.com/2013/09/30/how-to-check-if-a-port-is-open-or-not-in-c-unixlinux/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <Imlib2.h>

#define EVENT_LOOP_USEC_DELAY 50000
#define DEFAULT_PORT 80
#define DEFAULT_DELAY_SEC 5
#define PORT_TEST_INTERVAL_SEC 1

typedef struct Parameters {
  char* image_path;
  int delay_sec;
  char* host_name;
  int port;
} Parameters;


Display *display;
Screen *screen;
int screen_id;
Window window;
GC gc;

int alarm_counter = 0;

Parameters parameters;

// Event processing loop continues
// while the status is negative
int exit_status = -1;

void init_x();
void close_x();
void redraw();
void alarm_handler(int signum);
bool file_exists (char *filename);
void print_usage();
bool is_a_number(char* str);
bool is_port_open(char* hostname, int port_number);
bool parse_arguments(
  int argc, 
  char *argv[], 
  Parameters* parameters);

int main(int argc, char *argv[]) {
  
  if(!parse_arguments(argc, argv, &parameters)) {
    print_usage();
    exit(1);
  }
  
  init_x(parameters.image_path);

  signal(SIGALRM, alarm_handler);
  alarm(PORT_TEST_INTERVAL_SEC);

  XEvent event;  
  while(exit_status<0) {
    if(!XPending(display)) {
      usleep(EVENT_LOOP_USEC_DELAY);
      continue;
    }

    XNextEvent(display, (XEvent *)&event);

    if (event.type==Expose && event.xexpose.count==0) {
      redraw();
    }
  }

  close_x();
  exit(exit_status);
}

bool parse_arguments(
  int argc, 
  char *argv[], 
  Parameters* parameters) {

  parameters->host_name = NULL;
  parameters->port = DEFAULT_PORT;

  if(argc<2) {
    printf("ERROR: too few arguments\n\n");
    return false;
  }

  if (argc==2 && strcmp(argv[1], "--help")==0) {
    printf(
      "XSplashScreen shows a picture in a splashscreen window\n"
      "for a specified number of seconds or until a specified port is opened\n\n");
    return false;
  }

  if(argc>=2) {
    parameters->image_path = argv[1];
  
    if(!file_exists(parameters->image_path)) {
      printf(
        "ERROR: image file '%s' not found\n\n", 
        parameters->image_path);
      return false;
    }
  }

  if(argc>=3) {
    char* delay_str = argv[2];
    if (!is_a_number(delay_str)) {
      printf("ERROR: '%s' is not a valid delay length\n\n", delay_str);
      return false;
    }
    parameters->delay_sec = atoi(delay_str);  
  }
  else {
    parameters->delay_sec = DEFAULT_DELAY_SEC;    
    return true;
  }

  if(argc>=4) {
    parameters->host_name = argv[3];
  }
  else {
    return true;
  }

  if(argc==5) {
    if(!is_a_number(argv[4])) {
      printf("ERROR: '%s' is not a valid port number\n", argv[4]);
      return false;
    } 
    parameters->port = atoi(argv[4]);
  }
  
  if(argc>5) {
    printf("ERROR: too many arguments\n\n");
    return false;
  }  
}

void print_usage() {
  printf(    
    "Usage:\n"
    "xsplashscreen PICTURE_PATH [DELAY_SEC] [HOST] [PORT]\n\n"
    "  PICTURE_PATH is an absolute or relative path to image\n"
    "               to be displayed in the splashscreen window\n\n"
    "  DELAY_SEC    is an optional maximum delay in seconds before \n"
    "               the splashscreen window is closed and the program\n"
    "               exits. Default value is %d seconds\n\n"
    "  HOST         an optional hostname or address for port availability\n"
    "               checks that are repeated every %d seconds if \n"
    "               this parameter is specified. The script exits with 0\n"
    "               return code as soon as connection attempt succeeds.\n"
    "               If the parameter is specified but none of connection\n"
    "               attempts before timeout succeeds, the program exits with\n"
    "               return code of 2\n\n"
    "  PORT         is an optional port number. Default value is %d\n",
    DEFAULT_DELAY_SEC,
    PORT_TEST_INTERVAL_SEC,
    DEFAULT_PORT); 

}

bool is_a_number(char* str) {
  return 
    str != NULL & 
    (strspn(str, "0123456789") == strlen(str));
}

bool is_port_open(char* hostname, int port_number) {
  int sockfd;
  struct sockaddr_in serv_addr;
  struct hostent *server;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
      printf("ERROR opening socket");
      return false;
  }

  server = gethostbyname(hostname);

  if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      return false;
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,
        (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);

  serv_addr.sin_port = htons(port_number);
  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
      return false;
  } else {
      return true;
  }

  close(sockfd);
}

void alarm_handler(int signum) {
  alarm_counter += PORT_TEST_INTERVAL_SEC;

  if (parameters.host_name!=NULL) {
    if(is_port_open(
          parameters.host_name, 
          parameters.port)) {
      printf(
        "%s:%d is open\n", 
        parameters.host_name, 
        parameters.port);
      exit_status = 0;
      return;
    }
    else {
      printf(
        "%s:%d is closed\n", 
        parameters.host_name, 
        parameters.port);
    }
  }

  if(alarm_counter>=parameters.delay_sec) {        
    exit_status = parameters.host_name==NULL?0:2;
  }
  else {
    alarm(PORT_TEST_INTERVAL_SEC);
  }  
}

bool file_exists (char *filename) {
  struct stat   buffer;
  return (stat (filename, &buffer) == 0);
}

void init_x(char* image_path) {
  Imlib_Image img = imlib_load_image(image_path);

  imlib_context_set_image(img);
  int width = imlib_image_get_width();
  int height = imlib_image_get_height();

  display = XOpenDisplay((char *)0);
  screen = DefaultScreenOfDisplay(display);
  screen_id = DefaultScreen(display);
  Window root_window = DefaultRootWindow(display);

  unsigned long black = BlackPixel(display, screen_id);
  unsigned long white=WhitePixel(display, screen_id);

  window = XCreateSimpleWindow(
    display,
    root_window,
    0,
    0,
    width,
    height,
    5,
    black,
    white);

  Pixmap pix = XCreatePixmap(
    display,
    window,
    width,
    height,
    DefaultDepthOfScreen(screen));

  Atom window_type = XInternAtom(display, "_NET_WM_WINDOW_TYPE", False);
  long value = XInternAtom(display, "_NET_WM_WINDOW_TYPE_SPLASH", False);
  XChangeProperty(
    display,
    window,
    window_type,
    XA_ATOM,
    32,
    PropModeReplace,
    (unsigned char *) &value,
    1);
  XSetStandardProperties(
    display,
    window,
    "XSplashScreen",
    "XSplashScreen",
    None,
    NULL,
    0,
    NULL);
  XSelectInput(display, window, ExposureMask);
  gc=XCreateGC(display, window, 0,0);

  imlib_context_set_display(display);
  imlib_context_set_visual(DefaultVisualOfScreen(screen));
  imlib_context_set_colormap(DefaultColormapOfScreen(screen));
  imlib_context_set_drawable(pix);

  imlib_render_image_on_drawable(0, 0);
  XSetWindowBackgroundPixmap(display, window, pix);
  XClearWindow(display, window);
  XMapRaised(display, window);
}

void close_x() {
  XFreeGC(display, gc);
  XDestroyWindow(display, window);
  XCloseDisplay(display);
};

void redraw() {
  XClearWindow(display, window);
};

